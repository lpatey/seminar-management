
$(function() {

  $('[data-href]').click(function() {
    location.href = $(this).attr('data-href');
  });
  
  setTimeout(function() {
    $('li.Share ').fadeIn();
  },2000);
  
  $('[data-confirm]').click(function() {
    if(confirm($(this).attr('data-confirm'))) {
      $($(this).attr('data-yes')).submit();
    }
  });
  
  $('.Tabs').each(function() {
    var lis = $('li', this);
    lis.mousedown(function() {
      lis.each(function() {
        $(this).removeClass('Selected');
        $($(this).attr('data-to')).hide();
      });
      $($(this).attr('data-to')).show();
      $(this).addClass('Selected');
    });
  
    $('li:first', this).mousedown();
  });
  
  $('.More').click(function() {
    if($(this).hasClass('Active')) {
      $('img', this).attr('src', 'images/right-arrow.png');
    }
    else {
      $('img', this).attr('src', 'images/down-arrow.png');
    }
    $(this).toggleClass('Active');
    $($(this).attr('data-target')).toggle();
  });
});

