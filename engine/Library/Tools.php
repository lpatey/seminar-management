<?php

class Library_Tools {

  public static function urlify($title) {
    $title = htmlentities($title, ENT_COMPAT, 'utf-8');
	  $title = str_replace(
	      array('&eacute;', '&egrave;', '&ecirc;', '&agrave;', '&acirc;', '&ocirc;'), 
	      array('e', 'e', 'e', 'a', 'a', 'o'),
	      $title);
	  $title = preg_replace('/(\W|&[^;]+;)+/', '_', $title);
	  $title = substr($title, 0, 40);
	  if(strlen($title) <= 40) {
	    return $title;
	  }
	  $pos = strrpos($title, '_');
	  $title = substr($title, 0, $pos);
	  return $title;
  }
}
