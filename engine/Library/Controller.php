<?php

class Library_Controller {

	private static $_instance;
	
	public static function getInstance() {
		if( self::$_instance == null ) {
			self::$_instance = new Library_Controller();
		}
		return self::$_instance;
	}
	
	private function __construct() {
		spl_autoload_register( array( $this, 'autoload' ) );
	}

	public function autoload( $class ) {
		$class = strtr( $class, '_', '/' );
		if( file_exists( $class . '.php' ) ) {
			require_once $class . '.php';
		}
	}
	
	public static function dispatch() {
	
		$sessionConf = Library_Config::get( 'session' );
		session_start( $sessionConf['name'] );
		session_write_close();
	
		$class = isset($_GET['action']) ? $_GET['action'] : 'Main';
		
		$class = 'Action_' . $class;
		
		try {
		
			if( !class_exists( $class, true ) ) {
				header('HTTP/1.0 404 Not Found');
				throw new Exception( "Page does not exists : " . $_GET['action'] );
			}
		
			$class = new $class( $_REQUEST );
			$result = $class->execute();
		}
		catch( Exception $e ) {
			$class = new Action_Exception( $e );
			$result = $class->execute();
		}
		
		if( $result === null ) {
			$class->view();
		}
		else {
			header( 'Content-type: application/json' );
			die( json_encode( $result ) );
		}
	}
	
	
}
