<?php

class Library_Database {

	private static $_db;

	public static function getDb() {
		if( self::$_db == null ) {
			$conf = Library_Config::get( 'db' );
			$dsn = $conf['driver'] . ':dbname=' . $conf['dbname'] . ';host=' . $conf['host'];
			try {
				self::$_db = new PDO( $dsn, $conf['username'], $conf['password'] );
				self::$_db->query('SET NAMES UTF8');
			}
			catch( PDOException $e ) {
				throw new Library_Exception( $e->getMessage() );
			}
		}
		return self::$_db;
	}
}
