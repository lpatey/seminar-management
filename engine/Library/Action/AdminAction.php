<?php

abstract class Library_Action_AdminAction extends Library_Action {

	public function __construct( $params ) {
		parent::__construct( $params );
		Library_Auth::check();
	}

}
