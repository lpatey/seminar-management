<?php

abstract class Library_Action_AdminPortalAction extends Library_Action_PortalAction {

	public function __construct( $params ) {
		parent::__construct( $params );
		Library_Auth::check();
	}
}
