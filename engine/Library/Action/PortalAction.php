<?php

abstract class Library_Action_PortalAction extends Library_Action {

	private $_view;
	private $_title;
	
	public function execute() {
		parent::execute();
		parent::setView( 'Action/Master' );
		$this->_view = strtr( get_class( $this ), '_', '/' );
	}

	public function setView( $view ) {
		$this->_view = $view;
		return $this;
	}
	
	public function setTitle( $title ) {
		$this->_title = $title;
		return $this;
	}
	
	public function getTitle() {
		return $this->_title;
	}
	
	public function getContent() {
		require_once 'View/' . $this->_view . '.html';
	}
}
