<?php

class Library_Auth {

  public static function check() {
    if (!isset($_SERVER['PHP_AUTH_USER'])) {
      self::_authenticate();
    }
    
    $admin = Library_Config::get('admin');
    foreach($admin['users'] as $user) {
      if($_SERVER['PHP_AUTH_USER'] == $user['login'] 
        && $_SERVER['PHP_AUTH_PW'] == $user['password'] ) {
        return;
      }
    }
    self::_authenticate();
  }
  
  private static function _authenticate() {
    header('WWW-Authenticate: Basic realm="Turing\'s realm"');
    header('HTTP/1.0 401 Unauthorized');
    throw new Exception('Unauthorized');
  }
}
