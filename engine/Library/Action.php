<?php

abstract class Library_Action {

	private $_view = null;
	private $_params = array();
	
	public function __construct( $params ) {
		$this->_params = $params;
	}

	public function execute() {
		$this->_view = strtr( get_class( $this ), '_', '/' );
	}
	
	public function view() {
		require 'View/' . $this->_view . '.html';
	}
	
	public function setView( $view ) {
		$this->_view = $view;
		return $this;
	}
	
	public function getDb() {
		return Library_Database::getDb();
	}
	
	public function getParams() {
		return $this->_params;
	}
	
	public function getParam( $param ) {
		return $this->_params[ $param ];
	}
	
	public function getParamOrDefault( $param, $default ) {
		if( $this->hasParam( $param ) ) {
			return $this->getParam( $param );
		}
		return $default;
	}
	
	public function hasParam( $param ) {
		return isset( $this->_params[ $param ] );
	}
	
	public function escape( $text, $type = 'html' ) {
	  switch($type) {
	    case 'html' :
		  return htmlentities($text, ENT_COMPAT, 'utf-8');
		  case 'xml' :
		  return $text;
		  //return xmlentities( $text );
		}
		return $text;
	}
	
	public function action( $class, $params = array() ) {
	
		$class = 'Action_' . $class;
		
		$class = new $class( $params );
		
		$result = $class->execute();
		
		if( $result === null ) {
			ob_start();
			$class->view();
			return ob_get_clean();
		}
		else {
			return $result;
		}
	}
	
	public function redirect( $path ) {
		header( 'Location: ' . $path );
		exit(0);
	}
}
