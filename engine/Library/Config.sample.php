<?php

class Library_Config {

	private static $_config = array(
	
	  'website' => array(
	    'title' => 'Computability Seminar at LIAFA'
	  ),
	  
	  'date' => array(
	    'datetime' => 'F j, Y \a\t g:i a'
	  ),
	  
	  'admin' => array(
	    'users' => array(
	      array( 'login' => 'admin', 'password' => 'password' )
	    )
	  ),
	
		'db' => array(
			'driver' => 'mysql',
			'host' => 'localhost',
			'dbname' => 'seminar',
			'username' => 'root',
			'password' => ''
		),
		
		'mcrypt' => array(
			'cipher' => MCRYPT_RIJNDAEL_256,
			'mode' => MCRYPT_MODE_CFB
		),
		
		'session' => array(
			'remembrance' => array(
				'name' => 'seminar',
				'ttl' => 2592000
			),
			'name' => 'seminar'
		)
	);
	
	public static function get($key) {
		return self::$_config[ $key ];
	}
}
