<?php

class Library_Exception extends Exception {

	public function __construct( $message ) {
		parent::__construct($message);
	}
	
	public function execute() {
		parent::execute();
		$this->setTitle( 'An exception occured' );
	}
}
