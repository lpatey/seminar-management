<?php

class Action_Event extends Library_Action_PortalAction {

  private $_event;

  public function execute() {
    parent::execute();
  }
  
  public function getEvent() {
	  if(!is_array($this->_event)) {
	    $query = 'SELECT 
	      e.*, a.*,
	      l.location_id,
	      l.short AS location,
	      s.name AS structure 
	    FROM events e
	    LEFT JOIN authors a ON e.author_id = a.author_id
	    LEFT JOIN locations l ON e.location_id = l.location_id
	    LEFT JOIN authors_structures ss ON ss.author_id = a.author_id
	    LEFT JOIN structures s ON s.structure_id = ss.structure_id
	    WHERE event_id = :event_id
	    GROUP BY event_id';
	    $stmt = $this->getDb()->prepare( $query );
		  $stmt->execute( array( ':event_id' => $this->getParam('event_id')) );
		  $this->_event = $stmt->fetch(PDO::FETCH_ASSOC);
	  }
	  return $this->_event;
	}

  public function getTitle() {
    $event = $this->getEvent();
	  return $event['title'];
	}
	
	public function getAbstract() {
	  $event = $this->getEvent();
	  return $event['abstract'];
	}
	
	public function getWebPage() {
	  $event = $this->getEvent();
	  return $event['webpage'];
	}
	
	public function getDate() {
	  $event = $this->getEvent();
	  $date = Library_Config::get('date');
	  return date($date['datetime'], strtotime($event['date']));
	}
	
	public function getAuthor() {
	  $event = $this->getEvent();
	  $author = $event['first_name'] . ' ' . $event['last_name'];
	  if($event['structure']) {
	    $author .= ' (' . $event['structure'] . ')';
	  }
	  return $author;
	}
	
	public function hasLocation() {
	  $event = $this->getEvent();
	  return $event['location_id'];
	}
	
	public function getLocationShort() {
	  $event = $this->getEvent();
	  return $event['location'] ? $event['location'] : 'Unknown';
	}
	
	public function getLocationUrl() {
	  $event = $this->getEvent();
	  $title = Library_Tools::urlify($event['location']);
	  return './l:' . $event['location_id'] . ':' . $title;
	}
	
}
