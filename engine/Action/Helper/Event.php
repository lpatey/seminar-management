<?php

class Action_Helper_Event extends Library_Action {
	
	public function getTitle() {
	  return $this->getParam('title');
	}
	
	public function getUrl() {
	  $title = $this->getTitle();
	  $title = Library_Tools::urlify($title);
	  return './e:' . $this->getParam('event_id') . ':' . $title;
	}
	
	public function getContent() {
	  return $this->getParam('content');
	}
	
	public function getLocation() {
	  return $this->getParam('location');
	}
	
	public function getDate() {
	  $date = Library_Config::get('date');
	  return date($date['datetime'], strtotime($this->getParam('date')));
	}
	
	public function getAuthor() {
	  $author = $this->getParam('first_name')
	    . ' ' . $this->getParam('last_name');
	  if($this->getParam('structure')) {
	    $author .= ' (' . $this->getParam('structure') . ')';
	  }
	  return $author;
	}
}
