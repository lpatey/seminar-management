<?php

class Action_Helper_Links extends Library_Action {

  private $_links;

	public function getLinks() {
	  if(!is_array($this->_links)) {
	    $query = 'SELECT * FROM links';
	    $stmt = $this->getDb()->prepare( $query );
		  $stmt->execute( array() );
		  $this->_links = $stmt->fetchAll(PDO::FETCH_ASSOC);
	  }
	  return $this->_links;
	}
}
