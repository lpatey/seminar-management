<?php

class Action_Helper_Years extends Library_Action {

  private $_years;

  public function getYears() {
	  if(!is_array($this->_years)) {
	    $query = 'SELECT DISTINCT EXTRACT(YEAR FROM `date`) - IF(EXTRACT(MONTH FROM `date`) < 9, 1, 0) as year FROM events
	    ORDER BY `date` DESC';
	    $stmt = $this->getDb()->prepare( $query );
		  $stmt->execute( array() );
		  $this->_years = $stmt->fetchAll(PDO::FETCH_ASSOC);
	  }
	  return $this->_years;
	}
}
