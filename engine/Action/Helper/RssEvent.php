<?php

class Action_Helper_RssEvent extends Library_Action {
	
	public function getTitle() {
	  return $this->getParam('title');
	}
	
	public function getUrl() {
	  $title = $this->getTitle();
	  $title = htmlentities($title);
	  $title = str_replace(
	      array('&eacute;', '&egrave;', '&ecirc;', '&agrave;', '&acirc;', '&ocirc;'), 
	      array('e', 'e', 'e', 'a', 'a', 'o'),
	      $title);
	  $title = preg_replace('/(\W|&[^;]+;)+/', '_', $title);
	  $title = substr($title, 0, 40);
	  $pos = strrpos($title, '_');
	  $title = substr($title, 0, $pos);
	  $title = '/e:' . $this->getParam('event_id') . ':' . $title;
	  return 'http://' . dirname($_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]) . $title;
	}
	
	public function getContent() {
	  return $this->getParam('content');
	}
	
	public function getPubDate() {
	  return date('r', strtotime($this->getParam('date')));
	}
	
	public function getDate() {
	  return date('Y-m-d', strtotime($this->getParam('date')));
	}
	
	public function getLocation() {
	  return $this->getParam('location');
	}
	
	public function getDescription() {
	  $desc = '';
	  if($this->getAuthor()) {
	    $desc .= 'By ' . $this->getAuthor() . ' ';
	  }
	  if($this->getLocation()) {
	    $desc .= 'in ' . $this->getLocation() . ' ';
	  }
	  $desc .= $this->getAbstract();
	  return $desc;
	}
	
	public function getGuid() {
	  return $this->getParam('event_id');
	}
	
	public function getAbstract() {
	  return $this->getParam('abstract');
	}
	
	public function getAuthor() {
	  $author = $this->getParam('first_name')
	    . ' ' . $this->getParam('last_name');
	  if($this->getParam('structure')) {
	    $author .= ' (' . $this->getParam('structure') . ')';
	  }
	  return $author;
	}
}
