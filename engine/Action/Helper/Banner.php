<?php

class Action_Helper_Banner extends Library_Action {

  public function getConfigTitle() {
	  $website = Library_Config::get('website');
	  return $website['title'];
	}
	
	public function getEvent() {
	  return $this->action('Helper_Event', $this->getParam('event'));
	}
	
	public function hasEvent() {
	  $event = $this->getParam('event');
	  return $event['event_id'];
	}
	
	public function isShort() {
	  return !$this->hasParam('event');
	}

}
