<?php

class Action_Main extends Library_Action_PortalAction {

  private $_links;
  private $_events;
  private $_nextEvent;

	public function execute() {
		parent::execute();
		$website = Library_Config::get('website');
		$this->setTitle( $website['title'] );
		
	}
	
	public function getEvent($event) {
	  return $this->action('Helper_Event', $event);
	}
	
	public function getNextEvent() {
	  if(!is_array($this->_events)) {
	    $query = 'SELECT
	      e.*,
	      a.*,
	      l.location_id,
	      l.short AS location,
	      s.name AS structure
	    FROM events e
	    LEFT JOIN authors a ON e.author_id = a.author_id
	    LEFT JOIN authors_structures ss ON ss.author_id = a.author_id
	        AND e.`date` BETWEEN 
	        IF(ss.start_date IS NULL, e.`date`, ss.start_date) 
	        AND IF(ss.end_date IS NULL, e.`date`, ss.end_date)
	    LEFT JOIN structures s ON s.structure_id = ss.structure_id
	    LEFT JOIN locations l ON e.location_id = l.location_id
	    WHERE e.`date` > CURDATE()
	    ORDER BY e.`date` ASC
	    LIMIT 0,1';
	    $stmt = $this->getDb()->prepare( $query );
		  $stmt->execute( array() );
		  $this->_nextEvent = $stmt->fetch(PDO::FETCH_ASSOC);
		}
		return $this->_nextEvent;
	}
	
	public function getEvents() {
	  if(!is_array($this->_events)) {
	    $query = 'SELECT
	      e.*,
	      a.*,
	      l.location_id,
	      l.short AS location,
	      s.name AS structure 
	    FROM events e
	    LEFT JOIN authors a ON e.author_id = a.author_id
	    LEFT JOIN authors_structures ss ON ss.author_id = a.author_id
	        AND e.`date` BETWEEN 
	        IF(ss.start_date IS NULL, e.`date`, ss.start_date) 
	        AND IF(ss.end_date IS NULL, e.`date`, ss.end_date)
	    LEFT JOIN structures s ON s.structure_id = ss.structure_id
	    LEFT JOIN locations l ON e.location_id = l.location_id
	    GROUP BY event_id
	    ORDER BY `date` DESC LIMIT 0,3';
	    $stmt = $this->getDb()->prepare( $query );
		  $stmt->execute( array() );
		  $this->_events = $stmt->fetchAll(PDO::FETCH_ASSOC);
	  }
	  return $this->_events;
	}
	
}
