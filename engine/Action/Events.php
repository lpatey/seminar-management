<?php

class Action_Events extends Library_Action_PortalAction {

  private $_events;
  private $_years;

  public function execute() {
    parent::execute();
  }
  
  public function getYear() {
    if($this->hasParam('year')) {
      return $this->getParam('year');
    }
    return intval(date('Y'));
  }
  
  public function getEvent($event) {
	  return $this->action('Helper_Event', $event);
	}
  
  public function getEvents() {
	  if(!is_array($this->_events)) {
	    $query = 'SELECT
	      e.*,
	      a.*,
	      l.location_id,
	      l.short AS location,
	      s.name AS structure
	    FROM events e
	    LEFT JOIN authors a ON e.author_id = a.author_id
	    LEFT JOIN authors_structures ss ON ss.author_id = a.author_id
	    LEFT JOIN structures s ON s.structure_id = ss.structure_id
	      	AND e.`date` BETWEEN 
	        IF(ss.start_date IS NULL, e.`date`, ss.start_date) 
	        AND IF(ss.end_date IS NULL, e.`date`, ss.end_date)
	    LEFT JOIN locations l ON e.location_id = l.location_id
	    WHERE `date` BETWEEN :from AND :to
	    GROUP BY event_id
	    ORDER BY `date` DESC';
	    $stmt = $this->getDb()->prepare( $query );
		  $stmt->execute( array(
		    ':from' => $this->getYear() . '-09-00',
		    ':to' => ($this->getYear() + 1) . '-08-31'
		  ) );
		  $this->_events = $stmt->fetchAll(PDO::FETCH_ASSOC);
	  }
	  return $this->_events;
	}
  
}
