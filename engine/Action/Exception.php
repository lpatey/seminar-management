<?php

class Action_Exception extends Library_Action_PortalAction {

	private $_exception;
	
	public function __construct( $exception = null ) {
		if( $exception === null ) {
			throw new Library_Exception( 'Unknown exception' );
		}
		$this->_exception = $exception;
	}
	
	public function getException() {
		return $this->_exception;
	}
}
