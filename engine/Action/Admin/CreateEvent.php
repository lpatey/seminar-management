<?php

class Action_Admin_CreateEvent extends Library_Action_AdminAction {

  public function execute() {

    $params = $this->getParams();
    
    $query = 'INSERT INTO events
    SET
      author_id = :author_id,
      location_id = :location_id,
      title = :title,
      abstract = :abstract,
      `date` = :date';
    $stmt = $this->getDb()->prepare($query);
    $stmt->execute(array(
      ':author_id' => $params['author_id'],
      ':location_id' => $params['location_id'],
      ':title' => $params['title'],
      ':abstract' => $params['abstract'],
      ':date' => date('Y-m-d H:i:s', strtotime($params['date']))
    ));
    
    $this->redirect('Admin');
  }
}
