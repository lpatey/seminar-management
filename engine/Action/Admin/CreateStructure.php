<?php

class Action_Admin_CreateStructure extends Library_Action_AdminAction {

  public function execute() {
    
    $params = $this->getParams();
    
    $query = 'INSERT INTO structures
    SET
      name = :name,
      website = :website';
    $stmt = $this->getDb()->prepare($query);
    $stmt->execute(array(
      ':name' => $params['name'],
      ':website' => $params['website']
    ));
    
    $this->redirect('Admin');
  }
}
