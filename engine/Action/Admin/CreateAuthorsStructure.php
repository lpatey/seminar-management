<?php

class Action_Admin_CreateAuthorsStructure extends Library_Action_AdminAction {

  public function execute() {
    
    $params = $this->getParams();
    
    $start_date = strtotime($params['start_date']);
    $end_date = strtotime($params['end_date']);
    
    $query = 'INSERT INTO authors_structures
    SET
      author_id = :author_id,
      structure_id = :structure_id,
      start_date = :start_date,
      end_date = :end_date
      ';
    $stmt = $this->getDb()->prepare($query);
    $stmt->execute(array(
      ':author_id' => $params['author_id'],
      ':structure_id' => $params['structure_id'],
      ':start_date' => $start_date ? date('Y-m-d', $start_date) : null,
      ':end_date' => $end_date ? date('Y-m-d', $end_date) : null
    ));
    
    $this->redirect('Admin');
  }
}
