<?php

class Action_Admin_UpdateStructure extends Library_Action_AdminAction {

  public function execute() {
    
    $params = $this->getParams();
    
    $query = 'UPDATE structures
    SET
      name = :name,
      website = :website
    WHERE structure_id = :structure_id';
    $stmt = $this->getDb()->prepare($query);
    $stmt->execute(array(
      ':structure_id' => $params['structure_id'],
      ':name' => $params['name'],
      ':website' => $params['website']
    ));
    
    $this->redirect('Admin');
  }
}
