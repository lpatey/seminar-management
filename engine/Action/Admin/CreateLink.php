<?php

class Action_Admin_CreateLink extends Library_Action_AdminAction {

  public function execute() {
    
    $params = $this->getParams();
    
    $query = 'INSERT INTO links
    SET
      title = :title,
      url = :url';
    $stmt = $this->getDb()->prepare($query);
    $stmt->execute(array(
      ':url' => $params['url'],
      ':title' => $params['title']
    ));
    
    $this->redirect('Admin');
  }
}
