<?php

class Action_Admin_CreateAuthor extends Library_Action_AdminAction {

  public function execute() {
    
    $params = $this->getParams();
    
    $query = 'INSERT INTO authors
    SET
      first_name = :first_name,
      last_name = :last_name,
      email = :email,
      webpage = :webpage';
    $stmt = $this->getDb()->prepare($query);
    $stmt->execute(array(
      ':first_name' => $params['first_name'],
      ':last_name' => $params['last_name'],
      ':email' => $params['email'],
      ':webpage' => $params['webpage']
    ));
    
    $this->redirect('Admin');
  }
}
