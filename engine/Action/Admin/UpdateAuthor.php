<?php

class Action_Admin_UpdateAuthor extends Library_Action_AdminAction {

  public function execute() {
    
    $params = $this->getParams();
    
    $query = 'UPDATE authors
    SET
      first_name = :first_name,
      last_name = :last_name,
      email = :email,
      webpage = :webpage
    WHERE author_id = :author_id';
    $stmt = $this->getDb()->prepare($query);
    $stmt->execute(array(
      ':author_id' => $params['author_id'],
      ':first_name' => $params['first_name'],
      ':last_name' => $params['last_name'],
      ':email' => $params['email'],
      ':webpage' => $params['webpage']
    ));
    
    $this->redirect('Admin');
  }
}
