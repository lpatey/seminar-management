<?php

class Action_Admin_UpdateLink extends Library_Action_AdminAction {

  public function execute() {
    
    $params = $this->getParams();
    
    $query = 'UPDATE links
    SET
      title = :title,
      url = :url
    WHERE link_id = :link_id';
    $stmt = $this->getDb()->prepare($query);
    $stmt->execute(array(
      ':link_id' => $params['link_id'],
      ':url' => $params['url'],
      ':title' => $params['title']
    ));
    
    $this->redirect('Admin');
  }
}
