<?php

class Action_Admin_CreateLocation extends Library_Action_AdminAction {

  public function execute() {
    
    $params = $this->getParams();
    
    $query = 'INSERT INTO locations
    SET
      short = :short,
      detailed = :detailed,
      url = :url';
    $stmt = $this->getDb()->prepare($query);
    $stmt->execute(array(
      ':short' => $params['short'],
      ':detailed' => $params['detailed'],
      ':url' => $params['url']
    ));
    
    $this->redirect('Admin');
  }
}
