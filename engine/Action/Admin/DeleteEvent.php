<?php

class Action_Admin_DeleteEvent extends Library_Action_AdminAction {

  public function execute() {

    $params = $this->getParams();
    $query = 'DELETE FROM events
    WHERE event_id = :event_id';
    $stmt = $this->getDb()->prepare($query);
    $stmt->execute(array(
      ':event_id' => $params['event_id']
    ));
    
    $this->redirect('Admin');
  }
}
