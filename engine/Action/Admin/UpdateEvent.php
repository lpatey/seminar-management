<?php

class Action_Admin_UpdateEvent extends Library_Action_AdminAction {

  public function execute() {

    $params = $this->getParams();
    
    $query = 'UPDATE events
    SET
      author_id = :author_id,
      location_id = :location_id,
      title = :title,
      abstract = :abstract,
      `date` = :date
    WHERE event_id = :event_id';
    $stmt = $this->getDb()->prepare($query);
    $stmt->execute(array(
      ':event_id' => $params['event_id'],
      ':author_id' => $params['author_id'],
      ':location_id' => $params['location_id'],
      ':title' => $params['title'],
      ':abstract' => $params['abstract'],
      ':date' => date('Y-m-d H:i:s', strtotime($params['date']))
    ));
    
    $this->redirect('Admin');
  }
}
