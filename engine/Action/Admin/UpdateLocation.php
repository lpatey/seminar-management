<?php

class Action_Admin_UpdateLocation extends Library_Action_AdminAction {

  public function execute() {
    
    $params = $this->getParams();
    
    $query = 'UPDATE locations
    SET
      short = :short,
      detailed = :detailed,
      url = :url
    WHERE location_id = :location_id';
    $stmt = $this->getDb()->prepare($query);
    $stmt->execute(array(
      ':location_id' => $params['location_id'],
      ':short' => $params['short'],
      ':detailed' => $params['detailed'],
      ':url' => $params['url']
    ));
    
    $this->redirect('Admin');
  }
}
