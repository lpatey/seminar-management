<?php

class Action_Admin_DeleteAuthorsStructure extends Library_Action_AdminAction {

  public function execute() {

    $params = $this->getParams();
    $query = 'DELETE FROM authors_structures
    WHERE authors_structure_id = :authors_structure_id';
    $stmt = $this->getDb()->prepare($query);
    $stmt->execute(array(
      ':authors_structure_id' => $params['authors_structure_id']
    ));
    
    $this->redirect('Admin');
  }
}
