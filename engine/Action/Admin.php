<?php

class Action_Admin extends Library_Action_AdminPortalAction {

  private $_links;
  private $_authors;
  private $_events;
  private $_locations;
  private $_structures;

	public function execute() {
		parent::execute();
		$this->setTitle('Administration Panel');
		
	}
	
	public function getEvent($event) {
	  return $this->action('Helper_Event', $event);
	}
	
	public function getAuthors() {
	  if(!is_array($this->_authors)) {
	    $query = 'SELECT * FROM authors';
	    $stmt = $this->getDb()->prepare( $query );
		  $stmt->execute( array() );
		  $this->_authors = $stmt->fetchAll(PDO::FETCH_ASSOC);
		  foreach($this->_authors as $i => $author) {
		    $query = 'SELECT * FROM authors_structures a
		    LEFT JOIN structures ss ON ss.structure_id = a.structure_id
		    WHERE author_id = :author_id';
		    $stmt = $this->getDb()->prepare( $query );
		    $stmt->execute( array( ':author_id' => $author['author_id'] ) );
		    $this->_authors[$i]['structures'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
		  }
	  }
	  return $this->_authors;
	}
	
	public function getLinks() {
	  if(!is_array($this->_links)) {
	    $query = 'SELECT * FROM links';
	    $stmt = $this->getDb()->prepare( $query );
		  $stmt->execute( array() );
		  $this->_links = $stmt->fetchAll(PDO::FETCH_ASSOC);
	  }
	  return $this->_links;
	}
	
	public function getEvents() {
	  if(!is_array($this->_events)) {
	    $filter = '';
	    $params = array();
	    if($search = $this->getEventSearch()) {
	      $params[':search'] = '%' . $search . '%';
	      $filter = 'WHERE title LIKE :search OR abstract LIKE :search OR date LIKE :search ';
	    }
	    $query = 'SELECT * FROM events ' . $filter . ' ORDER BY date DESC LIMIT 0,5';
	    $stmt = $this->getDb()->prepare( $query );
		  $stmt->execute( $params );
		  $this->_events = $stmt->fetchAll(PDO::FETCH_ASSOC);
	  }
	  return $this->_events;
	}
	
	public function getLocations() {
	  if(!is_array($this->_locations)) {
	    $query = 'SELECT * FROM locations';
	    $stmt = $this->getDb()->prepare( $query );
		  $stmt->execute( array() );
		  $this->_locations = $stmt->fetchAll(PDO::FETCH_ASSOC);
	  }
	  return $this->_locations;
	}
	
	public function getStructures() {
	  if(!is_array($this->_structures)) {
	    $query = 'SELECT * FROM structures';
	    $stmt = $this->getDb()->prepare( $query );
		  $stmt->execute( array() );
		  $this->_structures = $stmt->fetchAll(PDO::FETCH_ASSOC);
	  }
	  return $this->_structures;
	}
	
	public function getEventSearch() {
	  return isset($_GET['search']) ? $_GET['search'] : '';
	}
	
}
