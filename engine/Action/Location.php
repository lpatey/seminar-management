<?php

class Action_Location extends Library_Action_PortalAction {

  private $_location;

  public function execute() {
    parent::execute();
  }
  
  public function getLocation() {
	  if(!is_array($this->_location)) {
	    $query = 'SELECT * FROM locations
	    WHERE location_id = :location_id';
	    $stmt = $this->getDb()->prepare( $query );
		  $stmt->execute( array( ':location_id' => $this->getParam('location_id')) );
		  $this->_location = $stmt->fetch(PDO::FETCH_ASSOC);
	  }
	  return $this->_location;
	}

  public function getShort() {
    $location = $this->getLocation();
	  return $location['short'];
	}
	
	public function getDetailed() {
	  $location = $this->getLocation();
	  return $location['detailed'];
	}
	
	public function getUrl() {
	  $location = $this->getLocation();
	  return $location['url'];
	}
	
}
